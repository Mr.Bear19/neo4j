from django.contrib import admin as dj_admin
from django_neomodel import admin as neo_admin

from .models import *
# Register your models here.
#@admin.register(Book)
class BookAdmin(dj_admin.ModelAdmin):
    list_display = ('title','created')
neo_admin.register(Book, BookAdmin)

#@admin.register(Person)
class PersonAdmin(dj_admin.ModelAdmin):
    list_display = ('fullname', 'email','username')
neo_admin.register(Person, PersonAdmin)

