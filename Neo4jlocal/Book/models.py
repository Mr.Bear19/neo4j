from types import SimpleNamespace
from django.db import models
from django_neomodel import DjangoNode
from neomodel import (StructuredNode,StringProperty, UniqueIdProperty, DateProperty
                         ,EmailProperty, RelationshipTo,RelationshipFrom,Relationship)
from neomodel.properties import IntegerProperty
# Create your models here.
class Book(DjangoNode):
    ID = UniqueIdProperty()
    title = StringProperty(unique_index =True)
    created = DateProperty(default_now = True)


    class Meta:
        app_label = 'Book'
    

class Person(DjangoNode):
    uuid = UniqueIdProperty()
    fullname =StringProperty()
    username = StringProperty()
    age = IntegerProperty()
    password = StringProperty()
    email = EmailProperty(unique_index = True)
    
    author = RelationshipTo('Book','Author')
    friend = Relationship('Person','friend')
    class Meta:
        app_label = 'Book'


