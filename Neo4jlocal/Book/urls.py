from django.urls import path
from Book import views

app_name ='Book'
urlpatterns = [
    
    path('index',views.index, name ='index'),
    path('',views.Mylogin,name ='login'),
    path('logout',views.Mylogout, name='logout'),
    path('register',views.MyRegister, name = 'register'),
]
