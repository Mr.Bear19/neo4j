from os import name
from django.shortcuts import redirect, render
from .models import *
from django.contrib.auth import logout
# Create your views here.

def index(request):
    if request.method == 'POST':
        return render(request, 'Book/index.html')
    else:
        search = request.GET.get('check')
        
        user = Person.nodes.get_or_none(username = search)
        
        if user == None:
            return render(request,'Book/index.html',{'message':'tác giả bạn tìm không tồn tại'})
        else:
            books = user.author.all()
            print(books)
            for book in books:
                print(book.title)
            return render(request,'Book/index.html',{'books':books})
    
def Mylogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = Person.nodes.get_or_none(username=username)
        if user == None:
            #render(request,'Book/login.html',{'tinhao':'797'})
            return render(request,'Book/login.html',{'message':'Tài khoản của bạn không tồn tại'})
        else:
            if user.password == password:
                return redirect('Book:index')
            else:
                 return render(request,'Book/login.html',{'message':'mật khẩu của bạn không đúng'})
    return render(request,'Book/login.html')

def Mylogout(request):
    try:
        logout(request)
    except:
        pass
    return redirect('Book:login')

def MyRegister(request):
    if request.method == 'POST':
        fullname = request.POST.get('fullname')
        username = request.POST.get('username')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        user = Person.nodes.get_or_none(username=username)
        if user == None:
            if password == password2:
                new_person = Person(age = '18',username = username,password =password, fullname = fullname)
                new_person.save()
                return redirect('Book:login')
            else:
                return render(request,'Book/register.html',{'message':'mật khấu xác nhận bạn không đúng'})
        else:
            return render(request,'Book/register.html',{'message':'tài khoản đã tồn tại'})
    return render(request,'Book/register.html')